﻿using UnityEngine;
using System.Collections;

public class Panner : MonoBehaviour {
    public float xSpeed, ySpeed;

    private Material _mat;
	// Use this for initialization
	void Start () {
        _mat = this.GetComponent<Renderer>().material;
	}
	
	// Update is called once per frame
	void Update () {
        _mat.mainTextureOffset += new Vector2(xSpeed, ySpeed) * Time.deltaTime;
	}   
}
