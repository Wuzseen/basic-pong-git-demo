﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {
    public float startSpeed = 5f;
    public float speedFactor = 1.2f;

    public float yVelocityFactor = 7f;

    public float maxVelocity = 50f;

    private Rigidbody2D rgd;

    private Vector3 startPosition;

    // Use this for initialization
    void Start () {
        rgd = this.GetComponent<Rigidbody2D>();
        startPosition = this.transform.position;
        ResetBall();
    }

    public void ResetBall() {
        float rand = 1f;
        this.transform.position = startPosition;
        if(Random.Range(0f, 1f) > .5f) {
            rand = -1f;
        }
        rgd.velocity = Vector2.right * rand * startSpeed;
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("CeilingFloor")) {
            FlipY(other);
        }
        else {
            FlipX(other);
        }
    }

    void FlipY(Collider2D other) {
        Vector2 currentVelocity = rgd.velocity;
        float newVelocity = -1f * currentVelocity.y;
        currentVelocity.y = newVelocity;
        rgd.velocity = currentVelocity;
    }

    void FlipX(Collider2D other) {
        Vector2 currentVelocity = rgd.velocity;
        float newVelocity = -1f * speedFactor * currentVelocity.x;
        if(Mathf.Abs(newVelocity) > maxVelocity) {
            newVelocity = maxVelocity * newVelocity / Mathf.Abs(newVelocity);
        }
        float ballY = transform.position.y;
        float paddleY = other.transform.position.y;
        float diff = ballY - paddleY;
        currentVelocity.x *= -1f * speedFactor;
        currentVelocity.y = diff * yVelocityFactor;
        rgd.velocity = currentVelocity;
    }
}
