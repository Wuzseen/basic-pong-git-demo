﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour {
    public string InputAxis = "Vertical";
    public float moveSpeed = 5f;

    private Rigidbody2D rgd;

	// Use this for initialization
	void Start () {
        rgd = this.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        float vInput = Input.GetAxis(InputAxis);
        rgd.MovePosition((Vector2)transform.position + Vector2.up * moveSpeed * vInput * Time.deltaTime);
	}
}
