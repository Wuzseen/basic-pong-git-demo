﻿using UnityEngine;
using System.Collections;

public class ScoreWall : MonoBehaviour {
    public bool player1Scores;
    public Game game;
        
    void OnTriggerEnter2D(Collider2D other) {
        if(player1Scores == true) {
            game.ScorePlayer1();
        }
        else {
            game.ScorePlayer2();
        }
    }
}
