﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Game : MonoBehaviour {
    public Text scoreText;

    public Ball ball;

    private int player1Score, player2Score;
	// Use this for initialization
	void Start () {
        player1Score = 0;
        player2Score = 0;
	}
        
    void UpdateUI() {
        scoreText.text = player1Score.ToString() + " : " + player2Score.ToString();
    }

    public void ScorePlayer1() {
        player1Score++;
        ball.ResetBall();
        UpdateUI();
    }

    public void ScorePlayer2() {
        player2Score++;
        ball.ResetBall();
        UpdateUI();
    }
}
